package ru.archertech.lab;

import io.quarkus.runtime.Startup;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
@Startup
public class GreetingResource {

    @Inject
    PgPool pgPool;

    @PostConstruct
    public void initTestDate(){
        pgPool.query("INSERT INTO test(text) VALUES('123')").executeAndAwait();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello from RESTEasy Reactive";
    }

    @GET
    @Path("query-db")
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> queryDb() {
        return pgPool.query("SELECT * FROM test").execute()
                .onItem().transform(rows -> rows.iterator().next())
                .onItem().transform(row-> row.getString("text"));
    }
}